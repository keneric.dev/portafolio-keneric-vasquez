 <!-- [ navigation menu ] start -->
 <nav class="pc-sidebar ">
        <div class="navbar-wrapper">
            <div class="m-header">
                <a href="?op=permitido" class="b-brand" style="display: flex; align-items: center;">
                    <!-- ========   change your logo hear   ============ -->
                    <img src="public/images/utp.svg" style="width: 55px;height:auto;background:white;padding:5px; border-radius: 50px; margin-right:15px" alt="" class="logo logo-lg">
                    <h3 style="color:white;padding-top:10px"><span style="color: #6610f2;">UTP</span> ADMIN</h3>
                </a>
            </div>
            <div class="navbar-content">
                <ul class="pc-navbar">
                    <li class="pc-item pc-caption">
                        <label>Navegacion</label>
                        <span>UTP ADMIN</span>
                    </li>
                    <li class="pc-item">
                        <a href="?op=permitido" class="pc-link "><span class="pc-micon"><i
                                    data-feather="home"></i></span><span class="pc-mtext">Home</span></a>
                    </li>

                    <li class="pc-item">
                        <a href="?op=perfil" class="pc-link "><span class="pc-micon">
                            <i data-feather="user"></i></span><span class="pc-mtext">Mi Perfil</span></a>
                    </li>
                    

                    <li class="pc-item pc-hasmenu">
                        <a href="#!" class="pc-link "><span class="pc-micon"><i data-feather="box"></i></span><span
                                class="pc-mtext">Configuración</span><span class="pc-arrow"><i
                                    data-feather="chevron-right"></i></span></a>
                        <ul class="pc-submenu">
                            <li class="pc-item"><a class="pc-link" href="?op=admin">Usuarios</a></li>
                            
                        
                        </ul>
                    </li>
             
                    

                </ul>
                
            </div>
        </div>
    </nav>