
    <!-- Favicon icon -->
    <link rel="icon" href="public/images/utp_fondo.svg" type="image/x-icon" style="background:white;padding:5px; border-radius: 50px; margin-right:15px">

    <!-- font css -->
    <link rel="stylesheet" href="assets/fonts/font-awsome-pro/css/pro.min.css">
    <link rel="stylesheet" href="assets/fonts/feather.css">
    <link rel="stylesheet" href="assets/fonts/fontawesome.css">

    <!-- vendor css -->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/customizer.css">
    <link rel="stylesheet" href="public/css/loader.css">
    <link rel="stylesheet" href="public/css/profile.css">
