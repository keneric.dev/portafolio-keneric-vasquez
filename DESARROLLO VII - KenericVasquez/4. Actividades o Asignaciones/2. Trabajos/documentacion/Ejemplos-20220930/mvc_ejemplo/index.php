
<?php

//Incluyo los archivos necesarios
require("./model/Auto.php");
require("./controller/AutoController.php");

//Instancio el controlador
$controller = new AutoController;

//Decido la ruta en función de los elementos del array
if (isset($_GET['ver'])){

    $ver=$_GET['ver'];
    //Llamo al método ver pasándole la clave que me están pidiendo
    $controller->ver($ver);
}
else{

    //Llamo al método por defecto del controlador
    $controller->index();
}
