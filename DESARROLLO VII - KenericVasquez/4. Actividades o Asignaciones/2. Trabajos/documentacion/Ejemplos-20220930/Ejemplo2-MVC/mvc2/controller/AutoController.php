
<?php

class AutoController
{
    var $autos;

    function __construct()
    {
        $this->autos = [
            1 => new Auto("Wolkswagen","Polo","negro","Rebeca"),
            2 => new Auto("Toyota","Corolla","verde","Marcos"),
            3 => new Auto("Skoda","Octavia","gris","Mario"),
            4 => new Auto("Kia","Niro","azul","Jairo")
        ];
    }

    public function index(){

        //Asigno los autos a una variable que estará esperando la vista
        $rowset = $this->autos;


        //Le paso los datos a la vista
        require("view/index.php");

    }

    public function ver($id){

        if (array_key_exists($id,$this->autos)){

            //Si el elemento está en el array, lo muestro
            $row = $this->autos[$id];
            require("view/ver.php");
        }
        else{

            //Llamo al método por defecto del controlador
            $this->index();
        }

    }

}
